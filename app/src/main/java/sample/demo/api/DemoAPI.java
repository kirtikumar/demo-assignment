package sample.demo.api;


import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import sample.demo.model.UserDataVO;

public class DemoAPI {

    public static String BASE_URL = "http://instapreps.com/api/";

    public DemoAPI(String baseUrl) {
        BASE_URL = baseUrl;
    }

    public static AuthService authService = null;

    public static AuthService getAuthService() {

        if (authService == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            authService = retrofit.create(AuthService.class);
        }

        return authService;
    }


    public interface AuthService {
        @POST("enrollnow?")
        Call<UserDataVO> addUser(@Query("fullname") String fullname,
                                 @Query("mobileno") String mobileno,
                                 @Query("emailid") String emailid,
                                 @Query("message") String message);
    }
}
