package sample.demo.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import sample.demo.R;
import sample.demo.api.DemoAPI;
import sample.demo.model.UserDataVO;

import static sample.demo.api.DemoAPI.BASE_URL;

public class HomeFragment extends Fragment {

    @BindView(R.id.noDataFound)
    TextView noDataFound;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, v);
        progressBar.setVisibility(View.VISIBLE);
        // TODO this data can make dynamically now this is static
        Call<UserDataVO> authVOCall = new DemoAPI(BASE_URL).getAuthService().addUser("AnupRaaj", "9757418329", "anupraaj", "Chchahah");

        authVOCall.enqueue(new Callback<UserDataVO>() {
            @Override
            public void onResponse(Call<UserDataVO> call, retrofit2.Response<UserDataVO> response) {
                UserDataVO object = response.body();
                manageUi(object == null);
                manageAdapter(object);
            }

            @Override
            public void onFailure(Call<UserDataVO> call, Throwable t) {
                Log.d("", "");
                progressBar.setVisibility(View.GONE);
            }
        });

        return v;
    }

    private void manageAdapter(UserDataVO object) {
        UiAdapter adapter = new UiAdapter(getStringArray(object));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    private List<Pair<String, String>> getStringArray(UserDataVO object) {
        List<Pair<String, String>> list = new ArrayList<>();
        list.add(new Pair("Id", object.getId() == null ? "-" : object.getId()));
        list.add(new Pair("Full Name", object.getFullname() == null ? "-" : object.getFullname()));
        list.add(new Pair("Email Id", object.getEmailid() == null ? "-" : object.getEmailid()));
        list.add(new Pair("Message", object.getMessage() == null ? "-" : object.getMessage()));
        return list;
    }

    private void manageUi(boolean b) {
        if (b)
            noDataFound.setVisibility(View.VISIBLE);
        else
            noDataFound.setVisibility(View.GONE);
    }
}
