package sample.demo.ui;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sample.demo.R;

public class UiAdapter extends RecyclerView.Adapter<UiAdapter.ViewHolder> {
    private  List<Pair<String, String>> mItem;

    UiAdapter( List<Pair<String, String>> mItem) {
        this.mItem = mItem;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        Pair<String, String> data = mItem.get(position);

        if (position == 0) {
            // todo need to conver to pixel
            holder.parent.setPadding(300,10,20,20);
        }

        holder.textKey.setText(data.first);
        holder.text.setText(data.second);
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parent)
        ConstraintLayout parent;

        @BindView(R.id.textKey)
        TextView textKey;

        @BindView(R.id.text)
        TextView text;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
